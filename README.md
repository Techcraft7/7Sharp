![Logo](7Sharp/logo.png)
# 7Sharp
A simple shell!

## Lets make shelling a who lot simpler with 7Sharp!

# Contributing

1. Fork the project
2. `git clone https://www.github.com/You/7Sharp.git` clone
3. `git checkout -b AGoodName` use a good brach name
4. code and test
5. push to your fork `git add .`, then `git commit -m "CoolMessage"`,then `git push`
6. make a pull request
7. yay!
